INCLUDE_DIRS = ["include/claro", "include/claro/platform"]

CFLAGS = -Wall  -Wno-unused-variable -fno-common -D_MAC -DNO_CAIRO -I include/claro -I include/claro/platform -I include/claro/platform/cocoa
CC = gcc
LIBNAME =  build/libclaro.a
DYLIBNAME =  build/libclaro.dylib
DYLIBFLAGS = $(CFLAGS) -dynamiclib -undefined suppress -flat_namespace -framework Cocoa

CLARO_FILES=src/block.o \
  src/hashtable.o \
  src/claro.o \
  src/list.o \
  src/log.o \
  src/memory.o \
  src/object.o \
  src/oscompat.o \
  src/store.o \
  src/object_map.o \
  src/widgets/window.o \
  src/widgets/toolbar.o \
  src/widgets/textbox.o \
  src/widgets/button.o \
  src/widgets/splitter.o \
  src/widgets/container.o \
  src/widgets/checkbox.o \
  src/widgets/dialog.o \
  src/widgets/label.o \
  src/widgets/progress.o \
  src/widgets/radio.o \
  src/widgets/statusbar.o \
  src/widgets/textarea.o \
  src/widgets/frame.o \
  src/widgets/line.o \
  src/widgets/image.o \
  src/widgets/list.o \
  src/widgets/listbox.o \
  src/widgets/listview.o \
  src/widgets/combo.o \
  src/widgets/menubar.o \
  src/widgets/menu.o \
  src/widgets/scrollbar.o \
  src/widgets/workspace.o \
  src/widgets/opengl.o \
  src/widgets/treeview.o \
  src/widgets/stock.o \
  src/layout_parser.o \
  src/layout.o \
  src/layout_heap.o \
  src/widget.o \
  src/graphics.o \
  src/image.o \
  src/font.o \
  src/system.o \
  src/platform/macosx_cocoa.o \
  src/platform/cocoa/window.o \
  src/platform/cocoa/toolbar.o \
  src/platform/cocoa/textbox.o \
  src/platform/cocoa/button.o \
  src/platform/cocoa/splitter.o \
  src/platform/cocoa/container.o \
  src/platform/cocoa/label.o \
  src/platform/cocoa/statusbar.o \
  src/platform/cocoa/progressbar.o \
  src/platform/cocoa/checkbox.o \
  src/platform/cocoa/radio.o \
  src/platform/cocoa/textarea.o \
  src/platform/cocoa/frame.o \
  src/platform/cocoa/line.o \
  src/platform/cocoa/image.o \
  src/platform/cocoa/listview.o \
  src/platform/cocoa/listbox.o \
  src/platform/cocoa/combo.o \
  src/platform/cocoa/menubar.o \
  src/platform/cocoa/menu.o \
  src/platform/cocoa/scrollbar.o \
  src/platform/cocoa/workspace.o \
  src/platform/cocoa/opengl.o \
  src/platform/cocoa/treeview.o \
  src/platform/cocoa/filedialogs.o \
  src/platform/cocoa/dialog.o \
  src/platform/cocoa/font.o \
  src/platform/cocoa/images.o \
  src/platform/cocoa/system.o \
  src/platform/cocoa/RBSplitView.o \
  src/platform/cocoa/RBSplitSubview.o \

all: main lib dylib

main: $(CLARO_FILES)

lib:
	@if [ ! -d build ];then mkdir -p build;fi
	ar -rcs $(LIBNAME) $(CLARO_FILES)

dylib:
	$(CC) $(DYLIBFLAGS) $(CLARO_FILES) -o $(DYLIBNAME)

clean:
	@rm -f $(LIBNAME) $(CLARO_FILES)
