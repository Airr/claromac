$execon "-framework Cocoa -I./include build/libclaro.a  -DNO_CAIRO"

$header
  #include <claro/base.h>
  #include <claro/graphics.h>
$header

$typedef object_t* control

sub window_closed (obj as control, event as event_t ptr)
  claro_shutdown()
end sub

sub claro_init()
  claro_base_init()
  claro_graphics_init()
end sub

raw w as control, t as control, b as control
raw combo as control, lb as control

claro_init()

w = window_widget_create(0, new_bounds(0,0,600,400), 0)
window_set_title(w, "Hello World")
object_addhandler(w, "destroy", window_closed)

t = textbox_widget_create(w, new_bounds( 20, 20, 410, -1 ), 0)
textbox_set_text(t, "Welcome to AIR's Cocoa Demo.")

b = button_widget_create_with_label(w, new_bounds(450, 20, 90, -1),0, "Load")

combo = combo_widget_create(w, new_bounds(20, 54, 110, -1), 0)
combo_append_row(combo,"Apples")
combo_append_row(combo,"Oranges")
combo_append_row(combo,"Peaches")
textbox_set_text(combo,"Oranges")

lb = textarea_widget_create(w, new_bounds(20, 94, 560, 280), 0)
'listbox_append_row(lb,"Entry One")
textarea_set_text(lb, "Hello, World! Adding Text...")

widget_focus(b)

window_show(w)
window_focus(w)

claro_loop()

