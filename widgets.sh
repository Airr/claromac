#!/usr/bin/env bash

WIDGETS=($(grep  -oh [a-z_]*widget_create $PWD/include/claro/widgets/*.h | sed 's/_widget_create//g' | uniq | sort))

IDENT=($(grep -oh claro.graphics.widgets.[a-z]* $PWD/src/widgets/*.c | uniq | sort))

printf "%-20s%-20s\n" "CLAROMAC WIDGET" "CLAROMAC IDENTIFIER"
printf '=%.0s' {1..50};echo

for (( I = 0; I < ${#WIDGETS[@]}; ++I )); do
    printf "%-20s%s\n" ${WIDGETS[I]} ${IDENT[I]}
done 
