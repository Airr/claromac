# ClaroMac GUI library #



This is an OSX-Only fork of the Claro GUI library from Gian Perrone and Theo Julienne.

Claro was originally a set of cross-platform libraries which assisted programmers in writing applications which could be run on almost any platform without modification. 

ClaroMac is an updated version with a multitude of fixes applied to bring it in line with current Cocoa programming practices

This project aims to provide an easy to use GUI library for 64-bit OSX programming.

To build, just run "make" in the root directory.  Both a static library and a dylib will be created in the "build" folder.

-AIR.