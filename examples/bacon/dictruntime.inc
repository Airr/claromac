
'-----------------------------------------------------------------------------------

SETENVIRON "LANG", "C"

'-----------------------------------------------------------------------------------

SUB Show_About(object_t* obj, event_t* event)

    show_about()

END SUB

'-----------------------------------------------------------------------------------

SUB Hide_About(object_t* obj, event_t* event)



END SUB

'-----------------------------------------------------------------------------------

SUB Clear_Edit(object_t* obj, event_t* event)

    textarea_set_text(text, "")
    list_widget_remove_all(list)

END SUB

'-----------------------------------------------------------------------------------

SUB Get_Dicts(object_t* obj, event_t* event)

    LOCAL dat$, total$, cbox_val$
    LOCAL mydict TYPE int
    LOCAL dimension, i

    total$ = ""

    list_widget_remove_all(list)

    cbox_val$ = combo_get_text(drop)

    IF LEN(cbox_val$)>0 THEN

        OPEN CONCAT$(cbox_val$, ":2628") FOR NETWORK AS mydict

        ' Get the available dictionaries
        SEND "SHOW DB\n" TO mydict

        REPEAT
            RECEIVE dat$ FROM mydict
            total$ = CONCAT$(total$, dat$)
        UNTIL ISFALSE(WAIT(mydict, 500))

        SEND "QUIT\n" TO mydict
        CLOSE NETWORK mydict

        SPLIT total$ BY NL$ TO line$ SIZE dimension


        ' Put results into the list
        FOR i = 2 TO dimension - 3
            IF NOT(INSTR(line$[i], "exit")) THEN
                listbox_append_row(list, CONCAT$(MID$(line$[i], INSTR(line$[i], "\"") + 1, INSTRREV(line$[i], "\"") - INSTR(line$[i], "\"") - 1), \
                    " - ", LEFT$(line$[i], INSTR(line$[i], " ") - 1) ))
            END IF
        NEXT
    END IF
    listbox_select_item_by_index(list,0);
END SUB

'-----------------------------------------------------------------------------------

SUB Lookup_Word(object_t* obj, event_t* event)

    LOCAL dict$, cbox_val$, lbox_val$, def$
    LOCAL mydict TYPE int
    LOCAL i, display, dimension

    total$ = ""
    def$ = ""
    dict$ = ""

    window_set_title(mainWin, CONCAT$(wintitle$, " - \"", textbox_get_text(entry), "\""))

    textarea_set_text(text, "")
    textarea_set_text(text, "Fetching....")
    'SYNC

    lbox_val$ = list_widget_get_text(list)
    cbox_val$ = combo_get_text(drop)

    dict$ = MID$(lbox_val$, INSTRREV(lbox_val$, " ") + 1)
    PRINT dict$

    OPEN CONCAT$(cbox_val$, ":2628") FOR NETWORK AS mydict

    SEND CONCAT$("DEFINE ", dict$, " ",  textbox_get_text(entry), "\n") TO mydict


    REPEAT
        RECEIVE dat$ FROM mydict
        total$ = CONCAT$(total$, dat$)
        SLEEP 0005
    UNTIL ISFALSE(WAIT(mydict, 500))

    SEND "QUIT\n" TO mydict
    CLOSE NETWORK mydict

    textarea_set_text(text, "")
    display = FALSE

    SPLIT total$ BY NL$ TO line$ SIZE dimension

    ' Parse result on error codes and results
    FOR i = 0 TO dimension - 1

        IF EQUAL(LEFT$(line$[i], 1), ".") THEN
            def$ = CONCAT$(def$, NL$)
            display = FALSE
        END IF

        IF ISTRUE(display) AND LEN(line$[i]) > 1 THEN
            def$ = CONCAT$(def$, line$[i])
        END IF

        IF EQUAL(LEFT$(line$[i], 3), "501") THEN
            def$ = CONCAT$(def$, "Select a dictionary or use the 'all' option!")
            BREAK
        END IF

        IF EQUAL(LEFT$(line$[i], 3), "550") THEN
            def$ = CONCAT$(def$, "Invalid database!")
            BREAK
        END IF

        IF EQUAL(LEFT$(line$[i], 3), "552") THEN
            def$ = CONCAT$(def$, "No match found.")
            BREAK
        END IF

        IF EQUAL(LEFT$(line$[i], 3), "151") THEN
            def$ = CONCAT$(def$, "------------------------------\n")
            dict$ = MID$(line$[i], LEN(textbox_get_text(entry)) + 8)
            def$ = CONCAT$(def$, MID$(dict$, INSTR(dict$, "\"") + 1, INSTRREV(dict$, "\"") - INSTR(dict$, "\"") - 1) )
            def$ = CONCAT$(def$, "\n------------------------------\n")
            display = TRUE
        END IF
    NEXT
    textarea_set_text(text,def$)

    widget_focus(entry)

END SUB
