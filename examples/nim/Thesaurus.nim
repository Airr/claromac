import claromac, net, strutils

proc fetchClicked (obj: ptr ClaroObj, event: ptr Event) {.cdecl.}
proc clearClicked (obj: ptr ClaroObj, event: ptr Event) {.cdecl.}
    
const 
    FORM_WIDTH = 600
    FORM_HEIGHT = 500



var line = ""      

var bd = newBounds(16,0,FORM_WIDTH,FORM_HEIGHT)
var mainWin = newWindow(nil, bd, 0)
mainWin.setText("Thesaurus")
mainWin.addHandler("destroy",claroQuit)

# Create droplist part
var label1 = newLabel(mainWin, newBounds(16,16,100,22),0,"Servers")
var cbServers = newCombo(mainWin, newBounds(16,36,180,24),0)
var btnFetch = newButton(mainWin, newBounds(196,38,90,22), 0, "Fetch")
btnFetch.addHandler("pushed", fetchClicked)

# Create control panel
var label2 = newLabel(mainWin, newBounds(322,16,100,22),0,"Control")
label2.layout(akRight)

var btnAbout = newButton(mainWin, newBounds(316,38,90,22),0,"About")
btnAbout.layout(akRight)

var btnClear = newButton(mainWin, newBounds(406,38,90,22),0,"Clear")
btnClear.layout(akRight)
btnClear.addHandler("pushed",clearClicked)

var btnExit = newButton(mainWin, newBounds(496,38,90,22),0,"Exit")
btnExit.layout(akRight)
btnExit.addHandler("pushed",claro_quit)

# Create dictionary panel
var label3 = newLabel(mainWin, newBounds(16,76,90,22) ,0,"Dictionaries")
var lbDictionary = newListBox(mainWin, newBounds(16,100,FORM_WIDTH-36,152),0)
lbDictionary.layout(akWidth + akTop)

# Create txtResults part
var label4 = newLabel(mainWin,newBounds(16,262,90,22),0,"Translation")
label4.layout(akLeft)

var txtResults = newTextArea(mainWin, newBounds(16,286,FORM_WIDTH-36,152),0)
txtResults.layout(akFull)

# Create txtEntry and lookup button
var txtEntry = newTextBox(mainWin, newBounds(16,454,FORM_WIDTH-130,22),0)
txtEntry.layout(akWidth + akBottom)

var chkDefault = newButton(mainWin,newBounds(496,456,90,22),0,"Search")
chkDefault.layout(akRight + akBottom)

# Add stuff to ComboBox
for x in ["dict.org","dict.us.dict.org","dict0.us.dict.org"]:
    cbServers.append(x)
    
# Set ComboBox Default    
cbServers.selectItem(0)

# Show Window
mainWin.focus
mainWin.show

# Run main loop
claro_loop()

    
proc fetchClicked (obj: ptr ClaroObj, event: ptr Event) =
    var dictServer = $cbServers.getValue()
    var sock: Socket = newSocket()
    sock.connect(dictServer,Port(2628))
    lbDictionary.clearAll()
    
    sock.send("SHOW DB\c\l")
    while true:
        sock.readLine(line)
        if line.startsWith("220") or line.startsWith("110") or line.startsWith("."): continue
        if line.startsWith("250"): break
        var tmp = line.split(chr(34))
#         echo tmp[1]," ",tmp[0]
        lbDictionary.append(tmp[1])
        
    sock.send("QUIT\c\l")
    sock.close()
    
proc clearClicked (obj: ptr ClaroObj, event: ptr Event) =
    lbDictionary.clearAll()