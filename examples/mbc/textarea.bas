$NOMAIN
$INCLUDE "claro.inc"

sub on_selected(obj as control, event as claroevent)

end sub

sub on_keyDown(obj as control, event as claroevent)
    textarea_set_text(text, textbox_get_text(obj))
end sub

sub clearTextArea(obj as control, event as claroevent)
    dim a$
    a$=""
	textarea_set_text(text,a$)
end sub

sub addToTextArea(obj as control, event as claroevent)
    dim a$*(1024*1024)*10
    a$ = LoadFile$("/Users/riveraa/Downloads/pg2600.txt")
	textarea_set_text(text,a$)

end sub


raw bd as bounds, line_bd as bounds, lt as layout
raw w as control, text as control, b1 as control, b2 as control
raw tbox as control, b3 as control

CONST LIST_BOX = "[][_(20)|text|(20)]"
CONST BUTTONS = "[{16}][(20)|tbox|(30)|>b1|>b2|b3<|(20)][]"

CONST LAYOUT = JOIN$(2,LIST_BOX,BUTTONS)

Function main(argc as integer, argv as PCHAR ptr)
	claro_init()
	
	bd = new_bounds(0,0,600,400)
	
	w = window_widget_create(0, bd, 0)
	window_set_title(w, "TextArea Test")
	object_addhandler(w, "destroy", claro_quit)
	
	lt = layout_create( w, LAYOUT, *bd, 90, 25 )
	
	text = textarea_widget_create(w, lt_bounds(lt,"text"),0 )
	b1 = button_widget_create_with_label(w, lt_bounds(lt,"b1"),0,"Add" )
	b2 = button_widget_create_with_label(w, lt_bounds(lt,"b2"),0,"Clear" )
	b3 = button_widget_create_with_label(w, lt_bounds(lt,"b3"),0,"Retrieve" )
	tbox = textbox_widget_create(w, lt_bounds(lt,"tbox"),0)

    ' ENABLES KEY_DOWN CALLBACK NOTIFIER
    widget_set_notify(tbox,cNotifyKey)
    
	object_addhandler(b1,"pushed",addToTextArea)
	object_addhandler(b2,"pushed",clearTextArea)
    object_addhandler(tbox, "key_down",on_keyDown)
    ' object_addhandler(listbox,"selected",on_selected)
	
	window_focus(w)
	window_show(w)
	
	
	claro_loop()
End Function