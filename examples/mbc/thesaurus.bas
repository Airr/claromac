$NOMAIN
$INCLUDE "claro.inc"


'---GLOBALS
raw bd as bounds
raw mainWin as control, aboutWin as control, drop as control
raw serverbutton as control, fetch as control
raw about as control, clearbut as control, exbut as control
raw line1 as control, line 2 as control, line3 as control
raw label1 as control, label2 as control,label3 as control, label4 as control
raw list as control, text as control, entry as control, dflt as control


'---MAIN
Function main(argc as integer, argv as PCHAR ptr)
	claro_init()
	
	' Create main window
	bd = new_bounds(0,0,530,500)
	mainWin = window_widget_create(0, bd, 0)
	widget_set_text(mainWin, "Thesaurus")
	object_addhandler(mainWin, "destroy", claro_quit)
	
	' Create droplist part
	label1 = label_widget_create_with_text(mainWin, new_bounds(16,16,90, 24),0,"Servers")
	drop = combo_widget_create(mainWin, new_bounds(16,34,132,24),0)
	fetch = button_widget_create_with_label(mainWin, new_bounds(150,36,90,24), 0, "Refresh")
	
	' Create control panel
	label2 = label_widget_create_with_text(mainWin, new_bounds(260,16,90, 24),0,"Control")
	about = button_widget_create_with_label(mainWin, new_bounds(254,36,90,24),0,"About")
	clearbut = button_widget_create_with_label(mainWin, new_bounds(342,36,90,24),0,"Clear")
	exbut = button_widget_create_with_label(mainWin, new_bounds(430,36,90,24),0,"Exit")
	object_addhandler(exbut,"pushed",claro_quit)
	
	' Line Separator
	line1 = line_widget_create(mainWin, new_bounds(16, 70, 500, 1),0)
	
	' Create dictionary panel
	label3 = label_widget_create_with_text(mainWin, new_bounds(16, 83, 90, 24),0,"Dictionaries")
	list = listbox_widget_create(mainWin, new_bounds(16, 108, 500, 120),0)
	
	' Create text part
	label4 = label_widget_create_with_text(mainWin,new_bounds(16, 248, 90, 24),0,"Translation")
	text = textarea_widget_create(mainWin, new_bounds(16, 273, 500, 155),0)
	
	' Create entry and lookup button
	entry = textbox_widget_create(mainWin, new_bounds(16, 448, 440, 24),0)
	dflt = checkbox_widget_create_with_label(mainWin,new_bounds(470, 448, 40, 24),0,"All")
	
	
	combo_append_row(drop, "dict.tugraz.at")
	combo_append_row(drop, "dict.tu-chemnitz.de")
	combo_append_row(drop, "dict.trit.org")
	combo_append_row(drop, "www.lojban.org")
	combo_append_row(drop, "dict.arabeyes.org")
	combo_append_row(drop, "dict.saugus.net")
	combo_append_row(drop, "dictionary.bishopston.net")
	combo_append_row(drop, "la-sorciere.de")
	
	window_focus(mainWin)
	window_show(mainWin)
	
	
	claro_loop()
End Function