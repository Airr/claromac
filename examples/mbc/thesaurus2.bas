$NOMAIN
$INCLUDE "claro.inc"


'---GLOBALS
raw bd as bounds, lt as layout
raw mainWin as control, aboutWin as control, drop as control
raw serverbutton as control, fetch as control
raw about as control, clearbut as control, exbut as control
raw line1 as control, line 2 as control, line3 as control
raw label1 as control, label2 as control,label3 as control, label4 as control
raw list as control, text as control, entry as control, dflt as control



CONST FIRST = "[{16}][(16)|servers|control<|(40)] [(16)|drop|>fetch|>about|(20)|>clear|>exit|(16)][{16}][{1}line][{10}]"
CONST SECOND = "[(16)|label3][_(16)|list|(16)][{10}][(16)|label4|(16)][_(16)|text|(16)][][(16)|entry|(20)|>dflt|(16)][]"
CONST LAYOUT = FIRST SECOND
CONST FORM_WIDTH = 600
CONST FORM_HEIGHT = 500

'---MAIN
Function main(argc as integer, argv as PCHAR ptr)
	claro_init()

' Create main window
	bd = new_bounds(16,0,FORM_WIDTH,FORM_HEIGHT)
	mainWin = window_widget_create(0, bd, 0)
	widget_set_text(mainWin, "Thesaurus")
	object_addhandler(mainWin, "destroy", claro_quit)

	lt = layout_create( mainWin, LAYOUT, *bd, 90, 25 )

' Create droplist part
	label1 = label_widget_create_with_text(mainWin, lt_bounds(lt,"servers"),0,"Servers")
	drop = combo_widget_create(mainWin, lt_bounds(lt,"drop"),0)
	fetch = button_widget_create_with_label(mainWin, lt_bounds(lt,"fetch"), 0, "Refresh")
'
' Create control panel
	label2 = label_widget_create_with_text(mainWin, lt_bounds(lt,"control"),0,"Control")
	about = button_widget_create_with_label(mainWin, lt_bounds(lt,"about"),0,"About")
	clearbut = button_widget_create_with_label(mainWin, lt_bounds(lt,"clear"),0,"Clear")
	exbut = button_widget_create_with_label(mainWin, lt_bounds(lt,"exit"),0,"Exit")
	object_addhandler(exbut,"pushed",claro_quit)

' Line Separator
	line1 = line_widget_create(mainWin, lt_bounds(lt,"line"),0)

' Create dictionary panel
	label3 = label_widget_create_with_text(mainWin, lt_bounds(lt,"label3"),0,"Dictionaries")
	list = listbox_widget_create(mainWin, lt_bounds(lt,"list"),0)

' Create text part
	label4 = label_widget_create_with_text(mainWin,lt_bounds(lt,"label4"),0,"Translation")
	text = textarea_widget_create(mainWin, lt_bounds(lt,"text"),0)

' Create entry and lookup button
	entry = textbox_widget_create(mainWin, lt_bounds(lt,"entry"),0)
	dflt = checkbox_widget_create_with_label(mainWin,lt_bounds(lt,"dflt"),0,"All")

' Add stuff to ComboBox
	combo_append_row(drop, "dict.tugraz.at")
	combo_append_row(drop, "dict.tu-chemnitz.de")
	combo_append_row(drop, "dict.trit.org")
	combo_append_row(drop, "www.lojban.org")
	combo_append_row(drop, "dict.arabeyes.org")
	combo_append_row(drop, "dict.saugus.net")
	combo_append_row(drop, "dictionary.bishopston.net")
	combo_append_row(drop, "la-sorciere.de")

' Show Window
	window_focus(mainWin)
	window_show(mainWin)

' Run mail loop
	claro_loop()
End Function
