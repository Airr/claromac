$INCLUDE "claro.inc"

sub button_click(obj as control, event as event_t ptr)
	dim var as LPVOID, ret
	var = dialog_widget_create("Hello","Testing",kInformation)
	ret = dialog_widget_run(var)
	select case ret:
		case 1000:
			widget_set_text(t,"Ok was clicked.")
		case 1001:
			widget_set_text(t,"Cancel was clicked.")
	end select
end sub

sub radio_click(obj as control, event as event_t ptr)
	dim txt$
	txt$ = "This is a RadioGroup. Selected option: "
    select case obj
        case rb1:
        	concat(txt$,"1")
        case rb2:
        	concat(txt$,"2")
        case rb3:
        	concat(txt$,"3")
    end select
    listbox_append_row(list1,txt$)
    widget_set_text(lbl1, txt$)
end sub


raw w as control, t as control, b as control
raw combo as control, lb as control
raw bd as bounds, line_bd as bounds, line_lt as layout, lt as layout
raw rg as control, rb1 as control, rb2 as control, rb3 as control
raw lbl1 as control, lbl2 as control,chk1 as control, chk2 as control, chk3 as control
raw line as control, list1 as control, menubar as control, menu as control
dim mitem1 as list_item_t ptr

raw m as control, mi as list_item_t ptr, msi as list_item_t ptr

claro_init()

bd = new_bounds(0,0,600,400)

w = window_widget_create(0, bd, 0)
'window_set_title(w, "OSX LIBCLARO")
widget_set_text(w, "OSX LIBCLARO")
object_addhandler(w, "destroy", claro_quit)

m = menubar_widget_create( w, 0 )
mi = menubar_append_item( m, 0, 0, "File" )
msi = menubar_append_item( m, mi, 0, "Open" )
menubar_add_key_binding(m,msi,"o",cModifierCommand)
msi = menubar_append_item( m, mi, 0, "Save" )
menubar_add_key_binding(m,msi,"s",cModifierCommand)


menubar_append_separator( m, mi )
msi = menubar_append_item( m, mi, 0, "Quit" )
menubar_add_key_binding(m,msi,"q",cModifierCommand)
object_addhandler( (control)msi, "pushed", claro_quit )
'** widget_hide(m)

CONST TOP_ROW = "[][(20)|textbox1 |(20)| button1(90)|(20)]"
CONST LINE = "[][(20)|line|(20)]"
CONST LABELS = "[(20)|label1|(60)|label2]"
CONST RADIO_CHKBOX = "[(20)|r1|(60)|check1] [(20)|r2|(60)|check2] [(20)|r3|(60)|check3]"
CONST LIST_BOX = "[][_(20)|list1|(20)][]"
CONST LAYOUT = JOIN$(5,TOP_ROW,  LINE, LABELS, RADIO_CHKBOX, LIST_BOX)

lt = layout_create( w, LAYOUT, *bd, 24, 24 )

t = textbox_widget_create(w, lt_bounds( lt, "textbox1" ), 0)

b = button_widget_create_with_label(w, lt_bounds( lt, "button1" ),0, "Load")
object_addhandler(b, "pushed", button_click)


line = line_widget_create(w, lt_bounds(lt, "line"), 0)
lbl1 = label_widget_create_with_text(w, lt_bounds( lt, "label1"),0, "This is a RadioGroup")
lbl2 = label_widget_create_with_text(w, lt_bounds( lt, "label2"),0, "These are CheckBoxes")

rg = radiogroup_create( w, 0 )
rb1 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r1"), "Option 1", 0 )
object_addhandler(rb1, "selected", radio_click)
rb2 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r2"), "Option 2", 0 )
object_addhandler(rb2, "selected", radio_click)
rb3 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r3"), "Option 3", 0 )
object_addhandler(rb3, "selected", radio_click)

chk1 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check1"),0, "Apples")
chk2 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check2"),0, "Oranges")
chk3 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check3"),0, "Peaches")

list1 = listbox_widget_create(w, lt_bounds(lt,"list1"),0 )
listbox_append_row(list1,"First")
listbox_append_row(list1,"Second")
listbox_append_row(list1,"Third")
widget_focus(b)
window_focus(w)
window_show(w)


claro_loop()
