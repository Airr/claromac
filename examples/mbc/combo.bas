$NOMAIN
$INCLUDE "claro.inc"

sub combo_callback(obj as control, event as claroevent)
	dim a$

	a$ = combo_get_text(obj)
	if len(a$) then
		widget_set_text(tbox, a$)
	end if
end sub

sub checkbox_callback(obj as control, event as claroevent)

	select case checkbox_get_checked(obj)
		case TRUE:
			widget_disable(tbox)
			widget_set_text(b1,"Enable TextBox")
		case FALSE:
			widget_enable(tbox)
			widget_set_text(b1,"Disable TextBox")
	end select


end sub
sub set_combo_default(obj as control, item as integer)
	combo_select_item(cbox, list_widget_get_row(cbox,0,item))
end sub

sub setup_combo()
	combo_append_row(cbox, "dict.tugraz.at")
	combo_append_row(cbox, "dict.tu-chemnitz.de")
	combo_append_row(cbox, "dict.trit.org")
	combo_append_row(cbox, "www.lojban.org")
	combo_append_row(cbox, "dict.arabeyes.org")
	combo_append_row(cbox, "dict.saugus.net")
	combo_append_row(cbox, "dictionary.bishopston.net")
	combo_append_row(cbox, "la-sorciere.de")
end sub

raw bd as bounds, line_bd as bounds, lt as layout
raw w as control, listbox as control, b1 as control, b2 as control
raw cbox as control, tbox as control

CONST COMBO_BOX = "[][(20)|cbox(160)|(20)][{10}][(20)|tbox|(20)|(130)b1|(20)][]"
'CONST BUTTONS = "[{16}][(20)|tbox|(30)|>b1|>b2|b3<|(20)][]"
'
CONST LAYOUT = COMBO_BOX

Function main(argc as integer, argv as PCHAR ptr)
	claro_init()
	
	bd = new_bounds(0,0,600,400)
	
	w = window_widget_create(0, bd, 0)
	window_set_title(w, "Combo Test")
	object_addhandler(w, "destroy", claro_quit)
	
	lt = layout_create( w, LAYOUT, *bd, 90, 25 )
	
	cbox = combo_widget_create(w, lt_bounds(lt,"cbox"),0)
	tbox = textbox_widget_create(w, lt_bounds(lt,"tbox"),0)
	b1 = checkbox_widget_create_with_label(w, lt_bounds(lt,"b1"),0, "Disable Textbox")
	
	setup_combo()
	set_combo_default(cbox,0)
	
	object_addhandler(cbox,"selected",combo_callback)
	object_addhandler(b1,"changed",checkbox_callback)
	
	window_focus(w)
	window_show(w)
	
	
	claro_loop()
End Function