$execon "-framework Cocoa -lclaro -L ~/.local/lib -I ~/.local/include/"


$header
    #include <claro/base.h>
    #include <claro/graphics.h>
    typedef object_t* control;
    typedef bounds_t* bounds;
    typedef layout_t* layout;
    typedef event_t*  claroevent;
$header


sub claro_init()
  claro_base_init()
  claro_graphics_init()
end sub

sub claro_quit (obj as control, event as event_t ptr)
  claro_shutdown()
end sub

sub widget_set_text(obj as control, txt as string)
	dim objname$
	objname$ = obj->type
	select case objname$
		case "claro.graphics.widgets.window":
			window_set_title(obj,txt)
		case "claro.graphics.widgets.textbox":
			textbox_set_text(obj,txt)
		case "claro.graphics.widgets.label":
			label_set_text(obj, txt)
		case "claro.graphics.widgets.checkbox":
			checkbox_set_label(obj,txt)


	end select

end sub
