$NOMAIN
$INCLUDE "claro.inc"

sub on_selected(obj as control, event as claroevent)
	dim a$
	a$ = listbox_get_value(listbox)

	if len(a$) then
		widget_set_text(tbox,a$)
	end if
end sub

sub retrieve(obj as control, event as claroevent)
	raw li as list_item_t ptr
	dim a$
	li = listbox_get_selected(listbox)
	if li then
		a$ = (PCHAR)li->data[0]
		print a$
	end if
end sub

sub clearListbox(obj as control, event as claroevent)
	listbox_remove_all(listbox)
end sub

sub addToListbox(obj as control, event as claroevent)
	listbox_append_row(listbox,"First")
	listbox_append_row(listbox,"Second")
	listbox_append_row(listbox,"Third")
end sub


raw bd as bounds, line_bd as bounds, lt as layout
raw w as control, listbox as control, b1 as control, b2 as control
raw tbox as control, b3 as control

CONST LIST_BOX = "[][_(20)|listbox|(20)]"
CONST BUTTONS = "[{16}][(20)|tbox|(30)|>b1|>b2|b3<|(20)][]"

CONST LAYOUT = JOIN$(2,LIST_BOX,BUTTONS)

Function main(argc as integer, argv as PCHAR ptr)
	claro_init()
	
	bd = new_bounds(0,0,600,400)
	
	w = window_widget_create(0, bd, 0)
	window_set_title(w, "List Test")
	object_addhandler(w, "destroy", claro_quit)
	
	lt = layout_create( w, LAYOUT, *bd, 90, 25 )
	
	listbox = listbox_widget_create(w, lt_bounds(lt,"listbox"),0 )
	b1 = button_widget_create_with_label(w, lt_bounds(lt,"b1"),0,"Add" )
	b2 = button_widget_create_with_label(w, lt_bounds(lt,"b2"),0,"Clear" )
	b3 = button_widget_create_with_label(w, lt_bounds(lt,"b3"),0,"Retrieve" )
	tbox = textbox_widget_create(w, lt_bounds(lt,"tbox"),0)

	object_addhandler(b1,"pushed",addToListbox)
	object_addhandler(b2,"pushed",clearListbox)
	object_addhandler(b3, "pushed",retrieve)
	object_addhandler(listbox,"selected",on_selected)
	
	window_focus(w)
	window_show(w)
	
	
	claro_loop()
End Function