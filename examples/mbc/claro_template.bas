$NOMAIN
$INCLUDE "claro.inc"



raw bd as bounds, line_bd as bounds, lt as layout
raw w as control, listbox as control, b1 as control, b2 as control
raw cbox as control, b3 as control

CONST COMBO_BOX = "[][(20)|>cbox|(20)]"
'CONST BUTTONS = "[{16}][(20)|tbox|(30)|>b1|>b2|b3<|(20)][]"
'
CONST LAYOUT = JOIN$(1,COMBO_BOX)

Function main(argc as integer, argv as PCHAR ptr)
	claro_init()
	
	bd = new_bounds(0,0,600,400)
	
	w = window_widget_create(0, bd, 0)
	window_set_title(w, "List Test")
	object_addhandler(w, "destroy", claro_quit)
	
'	lt = layout_create( w, LAYOUT, *bd, 90, 25 )
	cbox = combobox_widget_create(w, lt_bounds(lt,"cbox"),0)
	

	
	window_focus(w)
	window_show(w)
	
	
	claro_loop()
End Function