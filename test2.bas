'$execon "-framework Cocoa -I./include -L./build -lclaro -DNO_CAIRO"
$execon "-framework Cocoa -I./include build/libclaro.a -DNO_CAIRO"

$header
    #include <claro/base.h>
    #include <claro/graphics.h>
    typedef object_t* control;
    typedef bounds_t* bounds;
    typedef layout_t* layout;
$header



sub window_closed (obj as control, event as event_t ptr)
  claro_shutdown()
end sub

sub SetText(obj as control, txt as string)
	dim objname$
	objname$ = obj->type
	select case objname$
		case "claro.graphics.widgets.window":
			window_set_title(obj,txt)
		case "claro.graphics.widgets.textbox":
			textbox_set_text(obj,txt)
		case "claro.graphics.widgets.label":
			label_set_text(obj, txt)
	
	end select

end sub

sub button_click(obj as control, event as event_t ptr)
  dim ret$ 
  ret$ = OpenDialog("Select a ZIP File","zip")
  if LEN(ret$) then
      SetText(t, ret$)
  end if
end sub

sub claro_init()
  claro_base_init()
  claro_graphics_init()
end sub

sub radio_click(obj as control, event as event_t ptr)
	dim txt$
	txt$ = "This is a RadioGroup. Selected option: "
    select case obj
        case rb1:
            SetText(lbl1, txt$ + "1")
        case rb2:
            SetText(lbl1, txt$ + "2")
        case rb3:
            SetText(lbl1, txt$ + "3")
    end select
end sub


raw w as control, t as control, b as control
raw combo as control, lb as control
raw bd as bounds, line_bd as bounds, line_lt as layout, lt as layout
raw rg as control, rb1 as control, rb2 as control, rb3 as control
raw lbl1 as control, lbl2 as control,chk1 as control, chk2 as control, chk3 as control
raw line as control

claro_init()

bd = new_bounds(0,0,600,400)

w = window_widget_create(0, bd, 0)
'window_set_title(w, "OSX LIBCLARO")
SetText(w, "OSX LIBCLARO")
object_addhandler(w, "destroy", window_closed)

CONST TOP_ROW = "[][(20)|textbox1 |(20)| button1(90)|(20)]"
CONST LABELS = "[(20)|line|(20)][(20)|label1|(60)|label2]"
CONST LINE = "[(20)|line|(20)]"
CONST RADIO_CHKBOX = "[(20)|r1|(60)|check1] [(20)|r2|(60)|check2] [(20)|r3|(60)|check3]"
CONST LAYOUT = JOIN$(4,TOP_ROW, LINE, LABELS, RADIO_CHKBOX)

lt = layout_create( w, LAYOUT, *bd, 24, 24 )

t = textbox_widget_create(w, lt_bounds( lt, "textbox1" ), 0)

b = button_widget_create_with_label(w, lt_bounds( lt, "button1" ),0, "Load")
object_addhandler(b, "pushed", button_click)

line_bd = new_bounds(0,0,600,400)

'line_lt = layout_create(w, LINE, *line_bd, 24,12)
line = line_widget_create(w, lt_bounds(lt, "line"), 0)
lbl1 = label_widget_create_with_text(w, lt_bounds( lt, "label1"),0, "This is a RadioGroup")
lbl2 = label_widget_create_with_text(w, lt_bounds( lt, "label2"),0, "These are CheckBoxes")

rg = radiogroup_create( w, 0 )
rb1 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r1"), "Option 1", 0 )
object_addhandler(rb1, "selected", radio_click)
rb2 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r2"), "Option 2", 0 )
object_addhandler(rb2, "selected", radio_click)
rb3 = radiobutton_widget_create( w, rg, lt_bounds(lt,"r3"), "Option 3", 0 )
object_addhandler(rb3, "selected", radio_click)

chk1 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check1"),0, "Apples")
chk2 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check2"),0, "Oranges")
chk3 = checkbox_widget_create_with_label(w, lt_bounds( lt, "check3"),0, "Peaches")
widget_focus(b)
window_focus(w)
window_show(w)


claro_loop()