#define _PLATFORM_INC
#include "graphics.h"
#include "platform/macosx_cocoa.h"




id cgraphics_dialog_widget_create(const char *title, const char *message, int alertType) {
    NSAlert *alert = [[[NSAlert alloc] init] autorelease];
     [alert addButtonWithTitle:@"OK"];
     [alert addButtonWithTitle:@"Cancel"];
     [alert setMessageText:[NSString stringWithUTF8String:title] ];
     [alert setInformativeText:[NSString stringWithUTF8String:message] ];
     [alert setAlertStyle: alertType];
     return alert;
}

int cgraphics_dialog_widget_run_modal(id dialog) {
	return [dialog runModal];
}