
#define _PLATFORM_INC
#include "graphics.h"
//#include <cairo-quartz.h>
#include "platform/macosx_cocoa.h"

const char* open_file_dialog(const char* title, const char* fileType) {
    NSString* path = @"";
    NSOpenPanel* widget = [NSOpenPanel openPanel];
    
    if (strlen(title)>0)
        widget.title = [NSString stringWithUTF8String: title];
        
    if (strlen(fileType)>0) 
        widget.allowedFileTypes = [NSArray arrayWithObject: [NSString stringWithUTF8String: fileType] ];
        
    widget.showsResizeIndicator = YES;
    widget.showsHiddenFiles = NO;
    widget.canChooseDirectories = NO;
    widget.canCreateDirectories = YES;
    widget.allowsMultipleSelection = NO;


    if ( [widget runModal] == NSModalResponseOK ) {
        path = [[widget URL] path];
        
    }
    return [path UTF8String];
}

const char* save_file_dialog(const char* title, const char* fileType) {
    NSString* path = @"";
	NSSavePanel *widget = [NSSavePanel savePanel];
	
    if (strlen(title)>0)
        widget.title = [NSString stringWithUTF8String: title];

    if (strlen(fileType)>0) 
        widget.allowedFileTypes = [NSArray arrayWithObject: [NSString stringWithUTF8String: fileType] ];
        
    widget.showsResizeIndicator = YES;
    widget.showsHiddenFiles = NO;
    widget.canCreateDirectories = YES;
            	
    if ( [widget runModal] == NSModalResponseOK ) {
        path = [[widget URL] path];
    }
	return [path UTF8String];

}