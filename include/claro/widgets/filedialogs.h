
#ifndef _CLARO_GRAPHICS_WIDGETS_FILEDIALOGS_H
#define _CLARO_GRAPHICS_WIDGETS_FILEDIALOGS_H

/** \defgroup file_dialogs FileDialogs Widget
 * \brief Open and Save Dialogs
 * @{
 */


/**
 * \brief Creates an Open FileDialog
 * 
 * \param title The title to display for the dialog, or NULL.
 * \param fileTypes A _SINGLE_ file extension to filter, or NULL.
 *
 * \return The full path to the selected file as a string, or NULL if the dialog is cancelled.
 */
const char* open_file_dialog(const char* title, const char* fileTypes);


/**
 * \description Creates a Save FileDialog
 * 
 * \param title The title to display for the dialog, or NULL.
 *
 * \return The full path to the file as a string, or NULL if the dialog is cancelled.
 */
const char* save_file_dialog(const char* title, const char* fileType);
/*\@}*/
#endif